"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// export const QUESTION_TYPES = {
//     "Drop Down": "6299b4ffe3d2004c0a545c36",
//     "Checkbox": "6299b4ffe3d2004c0a545c37",
//     "Text": "6299b4ffe3d2004c0a545c38",
//     "Numeric": "6299b4ffe3d2004c0a545c39",
// };
// ​
// export const ROLES = {
//     "ADMIN": "6299b4ffe3d2004c0a545c32",
//     "USER": "6299b4ffe3d2004c0a545c33"
// };
let questionType = '';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjlmMGNkYTU3Mzg1Y2JmZDBhMjQ4ODYiLCJuYW1lIjoiVGVqYXMgTGFiZGUiLCJlbWFpbCI6InRlamFzLmxhYmRlQGNvZGl0YXMuY29tIiwicGFzc3dvcmQiOiIkMmEkMTIkckcwaUZUSUhCNXJzeFhjaHUueEs3LnRta3VVcXFNZi51ZTJ3UWVvdlVDU1lpT013YUZlVEciLCJyb2xlIjoiNjI5OWI0ZmZlM2QyMDA0YzBhNTQ1YzMyIiwiaWF0IjoxNjU0NTkwNjk1LCJleHAiOjE2NTQ2NzcwOTV9.fre21Y1DBS9YGecm4VCTAnZKRFaXwKJVB9PV0-2qAcs';
const sendFormData = (data) => __awaiter(void 0, void 0, void 0, function* () {
    const response = yield fetch('https://forms-47.herokuapp.com/question', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json',
            'Authorization': token
        },
        body: JSON.stringify(data)
    });
    const responseData = yield response.json();
    // console.log(responseData);
    return responseData;
});
const getQuestions = () => __awaiter(void 0, void 0, void 0, function* () {
    const response = yield fetch('https://forms-47.herokuapp.com/question', {
        method: 'GET',
        headers: {
            'Authorization': token
        }
    });
    const responseData = yield response.json();
    // console.log('get data',responseData);
    return responseData;
});
