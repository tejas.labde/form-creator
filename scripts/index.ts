
const optionsArray:string[]=[];
let idNum=1;
let optionsEnabled=false;

const addOption=()=>{
    optionsEnabled=true;
    const inputBox=document.createElement('input');
    inputBox.setAttribute('placeholder','enter values');
    inputBox.id="option"+ idNum;
    idNum++;
    // console.log('value',inputBox.value);
    questionTemplate.appendChild(inputBox);
    // const deleteOption=document.createElement('span');
    // deleteOption.innerHTML='X';
    // questionTemplate.appendChild(deleteOption);

}


const createQues=(formData:questionData)=>{
    const questionBox=document.createElement('div');
    questionBox.classList.add('question-box');
    
    const arrowBtn=document.createElement('div');
    arrowBtn.classList.add('arrow-btns');

    const upBtn=document.createElement('p');
    upBtn.innerHTML='^';
    const downBtn=document.createElement('p');
    downBtn.innerHTML='v';
    arrowBtn.appendChild(upBtn);
    arrowBtn.appendChild(downBtn);

    const textBox=document.createElement('div');
    textBox.classList.add('text-box');
    const question=document.createElement('p');
    question.innerHTML=formData.text;
    textBox.appendChild(question);

    if(formData.options){
        for(let element of formData.options){
            const optionEl=document.createElement('p');
            optionEl.innerText=element;
            
            textBox.appendChild(optionEl);
        }
    }
   
    questionBox.appendChild(textBox);
    questionBox.appendChild(arrowBtn);

    formContainer.appendChild(questionBox);
    
}

const getData = async () =>{
    const getQuestionData=await getQuestions();
    for(let element of getQuestionData.data.questions){
        createQues(element);
    }
}


const sendData=()=>{
    if(optionsEnabled){
        const resp=sendFormData({type:questionType,text:questionText.value,options:optionsArray})
        alert(resp);
    }
    else{
        const resp=sendFormData({type:questionType,text:questionText.value})
        alert(resp);
    }
}
