"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const optionsArray = [];
let idNum = 1;
let optionsEnabled = false;
const addOption = () => {
    optionsEnabled = true;
    const inputBox = document.createElement('input');
    inputBox.setAttribute('placeholder', 'enter values');
    inputBox.id = "option" + idNum;
    idNum++;
    // console.log('value',inputBox.value);
    questionTemplate.appendChild(inputBox);
    // const deleteOption=document.createElement('span');
    // deleteOption.innerHTML='X';
    // questionTemplate.appendChild(deleteOption);
};
const createQues = (formData) => {
    const questionBox = document.createElement('div');
    questionBox.classList.add('question-box');
    const arrowBtn = document.createElement('div');
    arrowBtn.classList.add('arrow-btns');
    const upBtn = document.createElement('p');
    upBtn.innerHTML = '^';
    const downBtn = document.createElement('p');
    downBtn.innerHTML = 'v';
    arrowBtn.appendChild(upBtn);
    arrowBtn.appendChild(downBtn);
    const textBox = document.createElement('div');
    textBox.classList.add('text-box');
    const question = document.createElement('p');
    question.innerHTML = formData.text;
    textBox.appendChild(question);
    if (formData.options) {
        for (let element of formData.options) {
            const optionEl = document.createElement('p');
            optionEl.innerText = element;
            textBox.appendChild(optionEl);
        }
    }
    questionBox.appendChild(textBox);
    questionBox.appendChild(arrowBtn);
    formContainer.appendChild(questionBox);
};
const getData = () => __awaiter(void 0, void 0, void 0, function* () {
    const getQuestionData = yield getQuestions();
    for (let element of getQuestionData.data.questions) {
        createQues(element);
    }
});
const sendData = () => {
    if (optionsEnabled) {
        const resp = sendFormData({ type: questionType, text: questionText.value, options: optionsArray });
        alert(resp);
    }
    else {
        const resp = sendFormData({ type: questionType, text: questionText.value });
        alert(resp);
    }
};
